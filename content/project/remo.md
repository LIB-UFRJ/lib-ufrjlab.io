+++
# Date this page was created.
date = "2016-04-27"

# Project title.
title = "Rema rema remador"

# Project summary to display on homepage.
summary = "Várias remadas"

# Optional image to display on homepage (relative to `static/img/` folder).
image_preview = "Rowing_icon_png.png"

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["instrumentação", "barco"]

# Optional external URL for project (replaces project detail page).
external_link = ""

# Does the project detail page use math formatting?
math = false

# Optional featured image (relative to `static/img/` folder).
#[header]
# image = "Rowing_icon_png.png"
# caption = "My caption :smile:"

+++

Barquinhos, sensores e remos
